# LIS4368 - Advanced Web Application Development

## Sebastian Arcila

#### Assignment 5:

1. Create data directory for data access layer
2. Created ConnectionPool file that creates object of connection
3. Skill Sets 13-15



#### Data Entry with Server Side Validation

| Before | After |
| ------- | ------ |
| ![first](img/1st.png) | ![second](img/2nd.png) |

#### Insertion into Customer Table


| ![third](img/3rd.png) |


#### Skill Sets

| Skill Set 13 |
| ------- |
| ![ss13](img/ss13.png) |

| Skill Set 14 | Skill Set 15
| ------- | ------ |
| ![ss14](img/ss14.png) | ![ss15](img/ss15.png) |

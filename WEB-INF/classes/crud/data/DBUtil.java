//indecated where this file can be found
package crud.data;

//importing all classes inside of SQL package
import java.sql.*;


//database connection cleanup: the purpose of this class is to close statements after they've been opened
public class DBUtil
{
  public static void closeStatement(Statement s)
  {
    try {
      if (s != null) {
        s.close();
      }
    }
    catch (SQLException e) {
      System.out.println(e);
    }
  }

  public static void closePreparedStatement(Statement ps)
  {
    try {
      if (ps != null) {
        ps.close();
      }
    }
    catch (SQLException e) {
      System.out.println(e);
    }
  }

  public static void closeResultSet(ResultSet rs)
  {
    try {
      if (rs != null) {
        rs.close();
      }
    }
    catch (SQLException e) {
      System.out.println(e);
    }
  }
}

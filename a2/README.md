
# LIS4368 Advanced Web Applications Development

## Sebastian Arcila

### Assignment 2 Requirements:
     * Created a Database Servlet
     * Impemented C.R.U.D functionality to website
     * Screenshots of Skill Sets


#### Local Host Webpage Links
[LIS4368 Webpage ](http://localhost:9999/lis4368/index.jsp "LIS4368 Webpage")


#### Assignment Screenshots
| Hello Path |
| ------- |
| ![HELLOPATH](img/1st-image.png) |

| Say Hi |
| ------- |
| ![Say Hi](img/2nd-image.png) |

| Query Book |
| ------- |
| ![Choices](img/3rd-image.png) |

| Results |
| ------- |
| ![Results](img/4th-image.png) |



#### Skill Sets

| Skill Set 1 |
| ------- |
| ![ss1](img/ss1.png) |

| Skill Set 2 | Skill Set 3
| ------- | ------ |
| ![ss2](img/ss2.png) | ![ss3](img/ss3.png) |

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Sebastian Arcila - saa17b

#### Assignment 4 Requirements:

1. Implement the Model View Controller (MVC) architecture
2. Add server side validation to prevent null and empty values
3. Skill sets 10-12


##### Failed & Passed Server-Side Validation

| Failed | Passed |
| ------- | ------ |
| ![Failed validation](img/failed_server_validation.png) | ![Passed validation](img/passed_server_validation.png) |


#### Skill Sets

| Skill Set 10 |
| ------- |
| ![ss10](img/ss10.png) |

| Skill Set 11 | Skill Set 12
| ------- | ------ |
| ![ss11](img/ss11.png) | ![ss12](img/ss12.png) |

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Sebastian Arcila - saa17b

#### Project 2 Requirements:

1. Add Update, Read, Delete functionality
2. Include delete prompt
3. Use ArrayList to pass data to front end


##### Valid user form entry

| ------- |
| ![Failed validation](img/failed_server_validation.png) |


#### Passed validation

| ------- |
| ![Failed validation](img/failed_server_validation.png) |


#### Display data

| ------- |
| ![Failed validation](img/initial_state.png) |

#### Modify Form

| ------- |
| ![Failed validation](img/modifying_form.png) |

#### Modified data

| ------- |
| ![Failed validation](img/modified_data.png) |

#### Delete warning

| ------- |
| ![Failed validation](img/delete_prompt.png) |


#### Associated database changes

| ------- |
| ![Failed validation](img/terminal.png) |

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Sebastian Arcila - saa17b

#### Project 1 Requirements:

*Four Parts*

1. Validate data from client side using jQuery
2. Link to lis4368 Web App
3. Skill Set 7-9


*Link to Local LIS4368 web app*

[LIS4368 Web App Link](http://localhost:9999/lis4368/index.jsp)


#### Assignment Screenshots

*Home Page*
![Home Page](img/home-page.png)

*Failed Validation*
![Failed Validation](img/failed-validation.png)

*Passed Validation*
![Passed Validation](img/passed-validation.png)


#### Skill Sets

| Skill Set 7 |
| ------- |
| ![ss7](img/ss7.png) |

| Skill Set 8 Pt.1| Skill Set 8 Pt.2
| ------- | ------ |
| ![ss8-1](img/ss8-1.png) | ![ss8-2](img/ss8-2.png) |

| Skill Set 9 |
| ------- |
| ![ss9](img/ss9.png) |